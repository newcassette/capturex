﻿using Clavis_Capture.Models;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Table;
using Newtonsoft.Json;
using Messaging;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Threading.Tasks;
using System.Web.Mvc;
using CAPTUREX.Models;
using System.Text;

namespace Clavis_Capture.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index(string id)
        {
            if (id != null)
            {
                CloudStorageAccount cloudStorageAccount = Common.GetCloudStorageAccount();
                CloudTableClient tableClient = cloudStorageAccount.CreateCloudTableClient();
                CloudTable masterTable = tableClient.GetTableReference("master");
                CloudTable requestTable = tableClient.GetTableReference("requests");
                CloudTable headerTable = tableClient.GetTableReference("headers");
                TableOperation retrieve = TableOperation.Retrieve<MasterRequestEntity>("master", id);
                TableResult result = masterTable.Execute(retrieve);

                if (result.Result != null)
                {
                    MasterRequestEntity master = (MasterRequestEntity)result.Result;
                    master.requests = new List<RequestEntity>();
                    var requestQuery = new TableQuery<RequestEntity>().Where(
                        TableQuery.GenerateFilterCondition(
                            "PartitionKey", 
                            QueryComparisons.Equal, 
                            master.RowKey
                        )
                    );

                    foreach(RequestEntity request in requestTable.ExecuteQuery(requestQuery))
                    {
                        master.requests.Add(request);
                        request.headers = new List<HeaderEntity>();
                        var headerQuery = new TableQuery<HeaderEntity>().Where(
                            TableQuery.GenerateFilterCondition(
                                "PartitionKey",
                                QueryComparisons.Equal,
                                request.RowKey
                            )
                        );

                        foreach(HeaderEntity header in headerTable.ExecuteQuery(headerQuery))
                        {
                            request.headers.Add(header);
                        }
                    }
                    ViewBag.master = JsonConvert.SerializeObject(master);
                    return View();
                }
            }

             return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "CaptureX";

            return View();
        }

        [HttpPost]
        public ActionResult PostRequest(MasterRequestEntity master)
        {
            saveMaster(master);
            Task.Factory.StartNew(() => saveRequestsToDatabase(master));
         //   Task.Factory.StartNew(() => Parallel.ForEach(json, x => fetchRequest(x)));
            return Json(new { Guid = master.RowKey });
        }

        [HttpPost]
        public string GetResponse(GetModel json)
        {
            CloudStorageAccount cloudStorageAccount = Common.GetCloudStorageAccount();
            CloudTableClient tableClient = cloudStorageAccount.CreateCloudTableClient();

            CloudTable requestTable = tableClient.GetTableReference("requests");
            CloudTable headerTable = tableClient.GetTableReference("headers");

            TableOperation op = TableOperation.Retrieve<RequestEntity>(json.partitionKey, json.rowKey);
            TableResult result = requestTable.Execute(op);

            if (result.Result != null)
            {
                RequestEntity request = (RequestEntity) result.Result;
                request.headers = new List<HeaderEntity>();
                TableQuery<HeaderEntity> query = new TableQuery<HeaderEntity>().Where(TableQuery.GenerateFilterCondition("PartitionKey", QueryComparisons.Equal, request.RowKey));

                foreach (HeaderEntity header in headerTable.ExecuteQuery(query))
                {
                    request.headers.Add(header);
                }

                ResponseEntity response = fetchResponse(request);
              
                

                return JsonConvert.SerializeObject(response);
            }
            return null;
        }

        private void saveMaster(MasterRequestEntity master)
        {
            var masterGuid = Guid.NewGuid().ToString();
            CloudStorageAccount cloudStorageAccount = Common.GetCloudStorageAccount();
            CloudTableClient tableClient = cloudStorageAccount.CreateCloudTableClient();
            CloudTable masterTable = tableClient.GetTableReference("master");
            
            try
            {
                masterTable.CreateIfNotExists();
            }catch(Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            master.PartitionKey = "master";
            master.RowKey = masterGuid;
            TableOperation masterInsert = TableOperation.Insert(master);
            try
            {
                masterTable.Execute(masterInsert);
            }catch(Exception e)
            {
                Console.WriteLine(e.ToString());
            }
        }

        public ActionResult Requests()
        {
            return View();
        }

        public ResponseEntity fetchResponse(RequestEntity request)
        {
            try
            {
                WebResponse response = Fetcher.Fetch(request);
                if (response != null)
                {
                    ResponseEntity responseEntity = new ResponseEntity();
                    responseEntity.PartitionKey = request.RowKey;
                    responseEntity.RowKey = Guid.NewGuid().ToString();
                    responseEntity.contentLength = response.ContentLength;
                    responseEntity.contentType = response.ContentType;
                    responseEntity.headers = new List<HeaderEntity>();
                    foreach (string key in response.Headers)
                    {
                        string val = response.Headers[key];
                        HeaderEntity header = new HeaderEntity();
                        header.PartitionKey = responseEntity.RowKey;
                        header.RowKey = Guid.NewGuid().ToString();
                        header.name = key;
                        header.value = val;
                        responseEntity.headers.Add(header);                  
                    }
                    string ret = "";
                    if (response.ContentType.StartsWith("image"))
                    {
                        Stream stream = response.GetResponseStream();
                        StreamReader reader = new StreamReader(stream);
                        MemoryStream ms = new MemoryStream();
                        stream.CopyTo(ms);
                        var base64Data = Convert.ToBase64String(ms.ToArray());
                        ret = "data:"+response.ContentType+";base64," + base64Data;
                    }
                    else
                    {
                        Stream stream = response.GetResponseStream();
                        StreamReader reader = new StreamReader(stream);
                        ret = reader.ReadToEnd();       
                    }

                    responseEntity.response = ret;
                    return responseEntity;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            return null;
          }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public static async Task saveRequestsToDatabase(MasterRequestEntity master)
        {   
            CloudStorageAccount cloudStorageAccount = Common.GetCloudStorageAccount();
            CloudTableClient tableClient = cloudStorageAccount.CreateCloudTableClient();

            CloudTable headersTable = tableClient.GetTableReference("headers");
            CloudTable requestTable = tableClient.GetTableReference("requests");
  
            requestTable.CreateIfNotExists();
            headersTable.CreateIfNotExists();

            IList<RequestEntity> json = master.requests;
            foreach (RequestEntity request in json)
            {
                if (!master.types.ContainsKey(request.type)) master.types.Add(request.type, 0);
                else master.types[request.type] = master.types[request.type]++;
                
                request.PartitionKey = master.RowKey;
                TableBatchOperation headerBatchOp = new TableBatchOperation();
                foreach (HeaderEntity header in request.headers)
                {
                    header.PartitionKey = request.RowKey;
                    headerBatchOp.Insert(header);
                }
                TableOperation requestInsert = TableOperation.Insert(request);
                TableResult result = await requestTable.ExecuteAsync(requestInsert);
                RequestEntity r = (RequestEntity)result.Result;
                MessageHub.RequestReady("{\"request\":"+JsonConvert.SerializeObject(r)+"}");
                await headersTable.ExecuteBatchAsync(headerBatchOp);
            }
        }
    }
}