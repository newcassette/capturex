﻿using Microsoft.WindowsAzure.Storage.Table;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Clavis_Capture.Models
{
    public class ResponseEntity : TableEntity
    {
        public string response { get; set; }
        public IList<HeaderEntity> headers;
        public string contentType { get; set; }
        public long contentLength { get; set; }
        public ResponseEntity(){
            this.PartitionKey = "Response";
            this.RowKey = Guid.NewGuid().ToString();
        }
    }
}