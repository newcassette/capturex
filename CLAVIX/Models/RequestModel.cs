﻿using Microsoft.WindowsAzure.Storage.Table;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Clavis_Capture.Models
{
    public class MasterRequestEntity : TableEntity
    {
        public string method { get; set; }
        public string result { get; set; }
        public IList<RequestEntity> requests { get; set; }
        public Dictionary<String, int> types { get; set; }
        public string user { get; set; }
        public MasterRequestEntity()
        {
            this.PartitionKey = "Master";
            this.RowKey = Guid.NewGuid().ToString();
            this.types = new Dictionary<string, int>();    
        }
    }

    public class RequestEntity : TableEntity
    {
        public string method { get; set; }
        public string type { get; set; }
        public string url { get; set; }
        public string requestBody { get; set; }
        public string requestId { get; set; }
        public IList<HeaderEntity> headers { get; set; }
        public string response { get; set; }

        public RequestEntity()
        {
            this.RowKey = Guid.NewGuid().ToString();
        }
    }

    public class HeaderEntity : TableEntity
    {
        public HeaderEntity()
        {
            this.RowKey = Guid.NewGuid().ToString();
        }
        public string name { get; set; }
        public string value { get; set; }
    }
}