﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.SignalR;
using Microsoft.Owin;
using Microsoft.Owin.Security.Cookies;
using Owin;

[assembly: OwinStartupAttribute(typeof(Clavis_Capture.Startup))]
namespace Clavis_Capture
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            GlobalHost.Configuration.DefaultMessageBufferSize = 500;
            app.MapSignalR("/signalr", new HubConfiguration());
        }
    }
}
