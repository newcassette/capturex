﻿using System.Web;
using System.Web.Optimization;

namespace Clavis_Capture
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/site.css",
                      "~/Content/font-awesome.css",
                      "~/Content/prism.css",
                      "~/Content/jquery.fancytree-css/skin-common.css",
                      "~/Content/jquery.fancytree-css/skin-xp/ui.fancytree.min.css",
                      "~/Content/DataTables/css/jquery.dataTables.min.css"
                      ));

            bundles.Add(new ScriptBundle("~/bundles/admin").Include(
                "~/Scripts/jquery-ui-1.12.1.min.js",
                "~/Scripts/underscore.min.js",
                "~/Scripts/metisMenu.min.js",
                "~/Scripts/sb-admin-2.js",
                "~/Scripts/knockout-3.4.2.js",
                "~/Scripts/jquery.fancytree-all.min.js",
                "~/Scripts/DataTables/jquery.dataTables.min.js",
                "~/Scripts/jquery.mark.min.js",
                "~/Scripts/vkbeautify.js",
                "~/Scripts/prism.js"
                ));

            bundles.Add(new ScriptBundle("~/bundles/signalr").Include(
                "~/Scripts/jquery.signalR-2.2.1.min.js",
                "~/Scripts/signalr/hubs/hubs.js"
                ));
        }
    }
}
