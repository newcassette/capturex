﻿using Clavis_Capture.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;

namespace Clavis_Capture
{
    public class Fetcher
    {
        public static WebResponse Fetch(RequestEntity request)
        {
            HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(request.url);
            WebHeaderCollection headers = new WebHeaderCollection();
            webRequest.Method = request.method;
            String[] cookieArray = null;
            try
            {
                webRequest.AutomaticDecompression = DecompressionMethods.GZip;
                webRequest.CookieContainer = new CookieContainer();
 
                foreach (HeaderEntity headerEnt in request.headers)
                {
                    switch (headerEnt.name.ToLower())
                    {
                        case "user-agent":
                            webRequest.UserAgent = headerEnt.value;
                            break;
                        case "accept":
                            webRequest.Accept = headerEnt.value;
                            break;
                        case "referer":
                            webRequest.Referer = headerEnt.value;
                            break;
                        case "content-type":
                            webRequest.ContentType = headerEnt.value;
                            break;
                        case "cookie":
                            cookieArray = headerEnt.value.Split(';');
                            foreach(String cookieStr in cookieArray)
                            {                          
                                String[] split = cookieStr.Split('=');
                                webRequest.CookieContainer.Add(new Cookie(split[0].Trim(), split[1].Trim(), null, webRequest.RequestUri.Host));                  
                            }
                            break;
                        default:
                            webRequest.Headers.Add(headerEnt.name, headerEnt.value);
                            break;
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Data);
            }

            try
            {
                if (request.requestBody != null)
                {
                    byte[] postBytes = Encoding.UTF8.GetBytes(request.requestBody);

                    webRequest.ContentLength = postBytes.Length;
                    webRequest.GetRequestStream().Write(postBytes, 0, postBytes.Length);
                }
                else
                {
                    webRequest.ContentLength = 0;
                }
            }catch(Exception ex)
            {
                Console.WriteLine(ex);
            }

            WebResponse response = null;         
            
            try
            {
                response = webRequest.GetResponse();
           
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Data);
            }
            return response;
 
        }
    }
}