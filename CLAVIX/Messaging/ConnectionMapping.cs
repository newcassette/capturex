﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Clavis_Capture.Messaging
{
    public class ConnectionMapping
    {
        public string ConnectionId { get; set; }
        public string Name { get; set; }
    }
}