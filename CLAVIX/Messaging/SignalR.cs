﻿using System;
using System.Web;
using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;
using System.Threading.Tasks;
using Clavis_Capture.Models;
using Clavis_Capture.Messaging;

namespace Messaging
{
    [HubName("messageHub")]
    public class MessageHub : Hub
    {

        private static IHubContext hubContext = GlobalHost.ConnectionManager.GetHubContext<MessageHub>();

        public override Task OnConnected()
        {
            string name = Context.User.Identity.Name;
            Groups.Add(Context.ConnectionId, name);

            return base.OnConnected();
        }
        public static void RequestReady(string request)
        {
            hubContext.Clients.All.broadcastMessage(request);         
        }

        public static void SaveUser(string name)
        {
            
        }
    }
}